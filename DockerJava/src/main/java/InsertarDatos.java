import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.swing.JOptionPane;

@WebServlet("/InsertarDatos")
public class InsertarDatos extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -674328328657289605L;
	static Connection conexion=null;
	static Statement sentencia=null;
	static ResultSet rs=null;
	
	
	

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		//processRequest(request, response);
	}
	

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException, IOException {
		//processRequest(request, response);
		
		String dato = request.getParameter("dato_a_insertar");
		
		String mensaje= "";
		
		try {
			Class.forName("org.postgresql.Driver");
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, "No se pudo cargar el puente JDBC-ODBC.");
			mensaje = "ERROR AL EJECUTAR Class.forName(\\\"org.postgresql.Driver\\\")";
			request.setAttribute("mensaje", mensaje);
			return;
		}
		try { 
			conexion = DriverManager.getConnection("jdbc:postgresql://180.10.0.2:5432/docpostgres", "postgres", "mysecretpassword");
		} 
		catch (Exception er) {
			mensaje = "ERROR AL OBTENER LA CONEXION: " + "catch (Exception er)" + er.toString();
			request.setAttribute("mensaje", mensaje);
			System.out.println("catch (Exception er)" + er.toString());
			
		}
		
		try {
			String consulta="INSERT INTO tabla_docpg VALUES ('" + dato + "')";
			sentencia = conexion.createStatement();
			rs = sentencia.executeQuery(consulta);
			
			System.out.println("SE HA INSERTADO en la tabla tabla_docpg. " + "INSERT INTO tabla_docpg VALUES ('" + dato + "')");
			mensaje = "SE HA INSERTADO en la tabla tabla_docpg. " + "INSERT INTO tabla_docpg VALUES ('" + dato + "')";
			request.setAttribute("mensaje", mensaje);

			sentencia.close();
			conexion.close();
			//out.println("<a href=Nuevo.jsp>Agregar Nuevo</a>");
		} catch(Exception er){
			//JOptionPane.showMessageDialog(null,"Error de conexion" );
		}
		
		request.getRequestDispatcher("/insertardatos.jsp").forward(request, response);
		System.out.println("request.getRequestDispatcher(\"/insertardatos.jsp\").forward(request, response);");
	}
	
	/**
	* Returns a short description of the servlet.
	*/
	public String getServletInfo() {
		return "Short description";
	}

}