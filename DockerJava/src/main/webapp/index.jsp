<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="ISO-8859-1">
<title>Docker JAVA Example</title>
</head>
<body style="text-align:center;">
      
    <h1 style="color:blue;">
        Docker Example - JAVA & POSTGRESQL
    </h1>
      
    <h4>
        Digite la opcion que necesite para ver el comportamiento con la BD PostgresSQL
    </h4>

	<form name="form1" action="<%=request.getContextPath() %>/CrearTabla" method="post">
        <p>
			<input type="submit" class="button" name="CrearTabla" value="Existe Tabla tabla_docpg?" />
		</p>
    </form>
	
	<form name="form2" action="<%=request.getContextPath() %>/ObtenerDatos" method="post">
        <p>
			<input type="submit" class="button" name="ObtenerDatos" value="Obtener Datos" />
		</p>
    </form>
    
	<form name="form3" action="<%=request.getContextPath() %>/InsertarDatos" method="post">
    	<p>
			Datos a Insertar: <input name="dato_a_insertar" id="dato_a_insertar">
			<input type="submit" class="button" name="InsertarDatos" value="Insertar Dato" />
		</p>
    </form>
</body>
</html>
